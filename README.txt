
--------------------------------------------------------------------------------
                                Label Value
--------------------------------------------------------------------------------

Maintainers:
 * Dimitri Dujardin (dimduj), dimduj@gmail.com


Project homepage (currently sandbox): https://drupal.org/sandbox/dimduj/2268341


Installation
------------

 * Copy the whole labelvalue directory to your modules directory
   (e.g. DRUPAL_ROOT/sites/all/modules) and activate the labelvalue
   module.
 * Go to the content type admin page and choose a content type to add the field.
